(function( $ ) {
	'use strict';
	$(document).ready( function (e) {
	    $('#form-imc').submit(function (e) { 
	        var edad = $('#edad').val();
	        var altura = $('#altura').val();
	        var peso = $('#peso').val();        
	        $("#resultado-imc").text('Tu índice de masa corporal es de ' + calcImc(altura,peso));
	        if (edad >= 65 ) {
	        	$('#resultado-imc').after('<h5>Estado Nutricional Índice de Masa Corporal (IMC) Adulto mayor 65 y más años.</h5>'
										+'<ul><li>Enflaquecido /a: Menor de 23</li>'
										+'<li>Normal:  23,1 a 27,9</li>'
										+'<li>Sobrepeso:  28 a 31,9</li>'
										+'<li>Obeso:  32 o más</li></ul>');
	        } else {
	        	$('#resultado-imc').after('<h5>Estado Nutricional Índice de Masa Corporal (IMC) Adulto 20 a 64 años.</h5>'
										+'<ul><li>Enflaquecido /a: Menor de 18,5</li>'
										+'<li>Normal:  18,5 a 24,9</li>'
										+'<li>Sobrepeso:  25 a 29,9</li>'
										+'<li>Obeso:  30 o más</li></ul>');
	        };
	        return false;              
		})
	});

	//calculate BMI with peso and altura
	function calcImc (altura, peso){
	    return(Math.round((peso/(altura*altura)*1000)*100)/10);
	};

})( jQuery );
