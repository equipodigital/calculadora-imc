<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Calculadora_IMC
 * @subpackage Calculadora_IMC/public/partials
 */
?>


<form id="form-imc" method="post" class="form-horizontal">

  <div class="form-group">
    <label class="col-sm-2 control-label" for="exampleInputEmail1">Altura</label>
    <div class="col-sm-4 input-group">
      <input required class="form-control" placeholder="Ej: 170" type="number" name="Altura en centímetros" value="" size="4" maxlength="4" id="altura" />
      <span class="input-group-addon">cms.</span>
    </div>
    <div class="clearfix"></div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-2 control-label" for="exampleInputEmail1">Peso</label>
    <div class="col-sm-4 input-group">
      <input required class="form-control" placeholder="Ej: 68" type="number" name="Peso en kilos" value="" size="5" maxlength="5" id="peso"/>
      <span class="input-group-addon">kgs.</span>
    </div>
    <div class="clearfix"></div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label" for="exampleInputEmail1">Edad</label>
    <div class="col-sm-4 input-group">
      <input required class="form-control" placeholder="Ej: 25" type="number" name="Edad" value="" size="25" maxlength="3" id="edad"/> 
      <span class="input-group-addon">años.</span>
    </div>
    <div class="clearfix"></div>
  </div>

  <div class="clearfix"></div>
  
   <button type="submit" class="btn-block btn-primary">Calcular IMC</button>

</form> 


<h3 id="resultado-imc"></h3>