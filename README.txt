=== Calculadora IMC EVS ===
Tags: calculadora, imc, evs
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Calculadora Índice de Masa Corporal para web Elige Vivir Sano.

== Description ==

Calculadora Índice de Masa Corporal para web Elige Vivir Sano.

== Installation ==

1. Subir carpeta `calculadora-imc` al directorio `/wp-content/plugins/`
2. Activar el plugin a través del menú 'Plugins' en WordPress
3. Insertar el shortcode `[imc]` en el contenido de la página donde se desea mostrar la calculadora

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 1.0 =
* Versión inicial